# AutoDrill 1.2

Why stand around a drill all day? You've got better things to do!
Automated drills!

Continued mod of AutoDrill (Honshitsu and RedFrog) with German and for support 1.2+

[Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2403180152)

## Version 1.0

* Supporting RimWorld 1.2
* German support

## Donate

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/C0C43PQ0I)